<?php

namespace App\Events;


/**
 * Событие перевода средств от юзера к юзеру
 */
class UserBalanceTransferEvent extends AbstractUserBalanceEvent
{
    public const EVENT_KEY = 'user_balance.transfer';

    public function publishEventKey(): string
    {
        return self::EVENT_KEY;
    }
}
