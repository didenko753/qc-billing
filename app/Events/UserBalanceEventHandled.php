<?php

namespace App\Events;


/**
 * Ответное событие на проведение операции.
 */
class UserBalanceEventHandled extends AbstractUserBalanceEvent
{
    public const EVENT_KEY = 'user_balance_event_handled';

    public function publishEventKey(): string
    {
        return self::EVENT_KEY;
    }
}
