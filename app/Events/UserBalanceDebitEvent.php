<?php

namespace App\Events;


/**
 * Событие снятия средств у юзера
 */
class UserBalanceDebitEvent extends AbstractUserBalanceEvent
{
    public const EVENT_KEY = 'user_balance.debit';

    public function publishEventKey(): string
    {
        return self::EVENT_KEY;
    }
}
