<?php

namespace App\Events;


/**
 * Событие списания ранее зафиксированной
 * суммы и разблокирования счета
 */
class UserBalanceUnlockDebetEvent extends AbstractUserBalanceEvent
{
    public const EVENT_KEY = 'user_balance.unlock_debet';

    public function publishEventKey(): string
    {
        return self::EVENT_KEY;
    }
}
