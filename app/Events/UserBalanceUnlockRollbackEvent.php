<?php

namespace App\Events;


/**
 * Событие анлока баланса
 */
class UserBalanceUnlockRollbackEvent extends AbstractUserBalanceEvent
{
    public const EVENT_KEY = 'user_balance.unlock_rollback';

    public function publishEventKey(): string
    {
        return self::EVENT_KEY;
    }
}
