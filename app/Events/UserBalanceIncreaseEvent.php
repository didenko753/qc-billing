<?php

namespace App\Events;


/**
 * Событие зачисления средств юзеру
 */
class UserBalanceIncreaseEvent extends AbstractUserBalanceEvent
{
    public const EVENT_KEY = 'user_balance.increase';

    public function publishEventKey(): string
    {
        return self::EVENT_KEY;
    }
}
