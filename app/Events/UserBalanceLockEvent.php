<?php

namespace App\Events;


/**
 * Событие лока баланса
 */
class UserBalanceLockEvent extends AbstractUserBalanceEvent
{
    public const EVENT_KEY = 'user_balance.lock';

    public function publishEventKey(): string
    {
        return self::EVENT_KEY;
    }
}
