<?php


namespace App\Events;


use App\Services\Balance\UserBalanceEventDTO;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;
use Nuwber\Events\Event\Publishable;
use Nuwber\Events\Event\ShouldPublish;

abstract class AbstractUserBalanceEvent implements ShouldPublish
{
    use Dispatchable, InteractsWithSockets, SerializesModels, Publishable;

    /**
     * @var UserBalanceEventDTO
     */
    private $userBalanceDTO;

    /**
     * Create a new event instance.
     *
     * @param UserBalanceEventDTO $userBalanceDTO
     */
    public function __construct(UserBalanceEventDTO $userBalanceDTO)
    {
        $this->userBalanceDTO = $userBalanceDTO;
    }

    /**
     * @inheritDoc
     */
    public function publishEventKey(): string
    {
        throw new \Exception('Must initialize method for event');
    }

    /**
     * @inheritDoc
     */
    final public function toPublish(): array
    {
        return [
            'payload' => $this->getUserBalanceDTO()->getPayload()
        ];
    }

    /**
     * @return UserBalanceEventDTO
     */
    public function getUserBalanceDTO(): UserBalanceEventDTO
    {
        return $this->userBalanceDTO;
    }
}