<?php

namespace App\Observers;

use App\Exceptions\LockedBalanceChangeValueException;
use App\Models\Balance;

class BalanceObserver
{
    /**
     * Handle the balance "created" event.
     *
     * @param  Balance  $balance
     * @return void
     */
    public function created(Balance $balance)
    {
        //
    }

    /**
     * @param Balance $balance
     * @throws \Exception
     */
    public function saving(Balance $balance)
    {
        if($balance->isLocked() && $balance->isDirty('value')) {
            throw new LockedBalanceChangeValueException($balance);
        }
    }

    /**
     * Handle the balance "updated" event.
     *
     * @param  Balance  $balance
     * @return void
     */
    public function updated(Balance $balance)
    {
        //
    }

    /**
     * Handle the balance "deleted" event.
     *
     * @param  Balance  $balance
     * @return void
     */
    public function deleted(Balance $balance)
    {
        //
    }

    /**
     * Handle the balance "restored" event.
     *
     * @param  Balance  $balance
     * @return void
     */
    public function restored(Balance $balance)
    {
        //
    }

    /**
     * Handle the balance "force deleted" event.
     *
     * @param  Balance  $balance
     * @return void
     */
    public function forceDeleted(Balance $balance)
    {
        //
    }
}
