<?php

namespace App\Providers;

use App\Listeners\UserBalanceListener;
use App\Listeners\UserBalanceResponseListener;

class RabbitEventsServiceProvider extends \Nuwber\Events\RabbitEventsServiceProvider
{

    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'user_balance.*' => [
            UserBalanceListener::class,
        ],
        'user_balance_event_handled' => [
            UserBalanceResponseListener::class,
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function boot()
    {
        parent::boot();
        //
    }
}
