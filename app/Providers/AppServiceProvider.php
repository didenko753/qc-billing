<?php

namespace App\Providers;

use App\Models\Balance;
use App\Observers\BalanceObserver;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Balance::observe(BalanceObserver::class);
    }
}
