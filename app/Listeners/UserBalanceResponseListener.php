<?php

namespace App\Listeners;

use Illuminate\Support\Facades\Log;

/**
 * Обработчик событий ответных сообщений из рэббита.
 */
class UserBalanceResponseListener
{
    /**
     * Handle the event.
     *
     * @param array $payload
     * @return void
     */
    public function handle(array $payload)
    {
        info("Event response succesfully handled. Payload:" . json_encode($payload));
    }
}
