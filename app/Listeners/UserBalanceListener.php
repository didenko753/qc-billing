<?php

namespace App\Listeners;

use App\Events\UserBalanceDebitEvent;
use App\Events\UserBalanceEventHandled;
use App\Events\UserBalanceIncreaseEvent;
use App\Events\UserBalanceLockEvent;
use App\Events\UserBalanceTransferEvent;
use App\Events\UserBalanceUnlockDebetEvent;
use App\Events\UserBalanceUnlockRollbackEvent;
use App\Services\Balance\BalanceService;
use App\Services\Balance\UserBalanceEventDTO;
use App\Services\UsersRepository;
use Exception;
use Illuminate\Support\Facades\Log;

/**
 * Обработчик событий из рэббита связанных с балансом.
 */
class UserBalanceListener
{
    /**
     * @var BalanceService
     */
    private $balanceService;
    /**
     * @var UsersRepository
     */
    private $usersRepository;

    /**
     * UserBalanceListener constructor.
     * @param BalanceService $balanceService
     * @param UsersRepository $usersRepository
     */
    public function __construct(BalanceService $balanceService, UsersRepository $usersRepository)
    {
        $this->balanceService = $balanceService;
        $this->usersRepository = $usersRepository;
    }

    /**
     * Handle the event.
     *
     * @param string $eventName
     * @param array $data
     * @return void
     * @throws Exception
     * @throws \Throwable
     */
    public function handle(string $eventName, array $data)
    {
        $userId = data_get($data, 'payload.user_data.user_id', null);
        $amount = data_get($data, 'payload.amount', null);

        try {
            $user = $this->usersRepository->getById($userId);
            $balance = $user->balance;
            switch ($eventName) {
                case UserBalanceIncreaseEvent::EVENT_KEY:
                    $this->balanceService->increaseUserBalance($balance, $amount);
                    break;
                case UserBalanceDebitEvent::EVENT_KEY:
                    $this->balanceService->debetUserBalance($balance, $amount);
                    break;
                case UserBalanceTransferEvent::EVENT_KEY:
                    $recipientUserId = data_get($data, 'payload.recipient_data.user_id');
                    $balanceTo = $this->usersRepository->getById($recipientUserId)->balance;
                    $this->balanceService->transfer($balance, $balanceTo, $amount);
                    break;
                case UserBalanceLockEvent::EVENT_KEY:
                    $this->balanceService->lockUserBalance($balance, $amount);
                    break;
                case UserBalanceUnlockRollbackEvent::EVENT_KEY:
                    $this->balanceService->unlockRollbackUserBalance($balance);
                    break;
                case UserBalanceUnlockDebetEvent::EVENT_KEY:
                    $this->balanceService->unlockDebetUserBalance($balance);
                    break;
                default:
                    throw new Exception(
                        'Неустановленное событие:' . $eventName .
                        '. Data:' . json_encode($data)
                    );
            }
        } catch (\Throwable $exception) {
            Log::error(
                __METHOD__ .
                " Event:$eventName. Message: ". $exception->getMessage()
            );
            throw $exception;
        }

        /**
         * Отправить событие ответ,
         * об успешном проведении операции
         */
        $userBalanceDTO = new UserBalanceEventDTO($user);
        $userBalanceDTO
            ->setEventName($eventName)
            ->setAmount($amount);

        publish(
            new UserBalanceEventHandled($userBalanceDTO)
        );

        info("Event $eventName succesfully handled. Data:" . json_encode($data));
    }
}
