<?php


namespace App\Services\Balance;


use App\Models\User;

/**
 * DTO для событий изменения
 * баланса пользователя.
 * Обязанности:
 *  - хранит в себе данные для отправки в брокер сообщений
 *  - формирует массив данных для отправки
 */
class UserBalanceEventDTO
{
    /**
     * Пользователь, с чьим счетом происходит операция
     *
     * @var User
     */
    private $user;

    /**
     * Получатель, кому производится перевод
     *
     * @var User|null
     */
    private $recipient;

    /**
     * Наименование события, в котором используется объект
     *
     * @var string|null
     */
    private $eventName;

    /**
     * Сумма операции
     *
     * @var int|null
     */
    private $amount;

    /**
     * UserBalanceDTO constructor.
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Сумма может быть и пустой,
     * так как есть операции без суммы (лок, анлок например)
     *
     * @return int|null
     */
    public function getAmount(): ?int
    {
        return $this->amount;
    }

    /**
     * @param int|null $amount
     * @return UserBalanceEventDTO
     */
    public function setAmount(?int $amount): self
    {
        $this->amount = $amount;
        return $this;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @return array
     */
    public function getUserData(): array
    {
        return $this->getUser()->toArray();
    }

    /**
     * @return array
     */
    public function getRecipientData(): array
    {
        return ($this->getRecipient()) ? $this->getRecipient()->toArray() : [];
    }

    /**
     * Получить данные для отправки
     *
     * @return array
     */
    public function getPayload(): array
    {
        return [
            'user_data' => $this->getUserData(),
            'amount' => $this->getAmount(),
            'event_name' => $this->getEventName(),
            'recipient_data' => $this->getRecipientData(),
        ];
    }

    /**
     * @param string $eventName
     * @return UserBalanceEventDTO
     */
    public function setEventName(string $eventName): self
    {
        $this->eventName = $eventName;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getEventName(): ?string
    {
        return $this->eventName;
    }

    /**
     * @param User|null $recipient
     * @return UserBalanceEventDTO
     */
    public function setRecipient(?User $recipient): self
    {
        $this->recipient = $recipient;
        return $this;
    }

    /**
     * @return User|null
     */
    public function getRecipient(): ?User
    {
        return $this->recipient;
    }
}