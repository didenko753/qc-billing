<?php


namespace App\Services\Balance;


use App\Exceptions\NegativeBalanceChangeValueException;
use App\Models\Balance;

/**
 * Сервис, отвечающий за
 * работу с балансом юзера
 */
class BalanceService
{
    /**
     * Увеличит баланс юзера на указанную сумму
     *
     * @param Balance $balance
     * @param int $amount
     */
    public function increaseUserBalance(Balance $balance, int $amount)
    {
        $balance->update([
            'value' => $balance->value + $amount
        ]);
    }


    /**
     * Спишет с баланса пользователя указанную сумму
     *
     * @param Balance $balance
     * @param int $amount
     * @throws \Exception
     */
    public function debetUserBalance(Balance $balance, int $amount)
    {
        if(!self::isValueGreaterDebetAmount($balance, $amount)) {
            throw new NegativeBalanceChangeValueException($balance);
        }
        $balance->update(['value' => $balance->value - $amount]);
    }

    /**
     * Залочит баланс юзера
     *
     * @param Balance $balance
     * @param int $amount
     * @throws NegativeBalanceChangeValueException
     */
    public function lockUserBalance(Balance $balance, int $amount)
    {
        $balance->lock($amount);
    }

    /**
     * Разлочит баланс юзера,
     * откатит сумму для последующего списания
     *
     * @param Balance $balance
     */
    public function unlockRollbackUserBalance(Balance $balance)
    {
        $balance->unlock();
    }

    /**
     * Спишет разлочит баланс и спишет залоченную сумму
     *
     * @param Balance $balance
     * @throws \Exception|\Throwable
     */
    public function unlockDebetUserBalance(Balance $balance)
    {
        if (!self::isValueGreaterDebetAmount($balance, $balance->locked_debit)) {
            throw new NegativeBalanceChangeValueException($balance);
        }
        $balance->update([
            'value' => $balance->value - $balance->locked_debit,
            'locked_debit' => null
        ]);
    }

    /**
     * @param Balance $balance
     * @param int $amount
     * @return bool
     */
    public static function isValueGreaterDebetAmount(Balance $balance, int $amount): bool
    {
        return $balance->value >= $amount;
    }

    /**
     * Перевести средства с одного счета на другой
     *
     * @param Balance $balanceFrom
     * @param Balance $balanceTo
     * @param int $amount
     * @throws NegativeBalanceChangeValueException
     */
    public function transfer(Balance $balanceFrom, Balance $balanceTo, int $amount)
    {
        if(!self::isValueGreaterDebetAmount($balanceFrom, $amount)) {
            throw new NegativeBalanceChangeValueException($balanceFrom);
        }

        /**
         * В транзакции снимем средства
         * с одного счета, переведем на другой
         */
        \DB::transaction(function () use ($balanceFrom, $balanceTo, $amount) {
            $this->debetUserBalance($balanceFrom, $amount);
            $this->increaseUserBalance($balanceTo, $amount);
        });
    }
}