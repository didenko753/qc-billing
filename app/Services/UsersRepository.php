<?php


namespace App\Services;


use App\Models\User;

class UsersRepository
{
    public function getById(int $id): User
    {
        return User::findOrFail($id);
    }
}