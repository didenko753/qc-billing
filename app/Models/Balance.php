<?php

namespace App\Models;

use App\Exceptions\NegativeBalanceChangeValueException;
use App\Services\Balance\BalanceService;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * App\Models\Balance
 *
 * @property int $balance_id
 * @property int $user_id
 * @property int $value
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Balance newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Balance newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Balance query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Balance whereBalanceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Balance whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Balance whereLocked($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Balance whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Balance whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Balance whereValue($value)
 * @mixin \Eloquent
 * @property int|null $locked_debit
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Balance whereLockedDebit($value)
 */
class Balance extends Model
{
    /**
     * @var string
     */
    protected $primaryKey = 'balance_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'value',
        'user_id',
        'locked_debit'
    ];

    /**
     * @return BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    /**
     * Залочит баланс установив сумму для дальнейшего списания
     *
     * @param int $amount
     * @throws NegativeBalanceChangeValueException
     */
    public function lock(int $amount)
    {
        if(!BalanceService::isValueGreaterDebetAmount($this, $amount)) {
            throw new NegativeBalanceChangeValueException($this);
        }

        $this->locked_debit = $amount;
        $this->save();
    }

    /**
     * Разлочит баланс, установив
     * пустую сумму для дальнейшего сняти
     */
    public function unlock()
    {
        $this->locked_debit = null;
        $this->save();
    }

    /**
     * Заполнен ли locked_debit
     *
     * @return bool
     */
    public function isLocked(): bool
    {
        return !is_null($this->locked_debit);
    }
}
