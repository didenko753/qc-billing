<?php

namespace App\Exceptions;

use App\Models\Balance;
use Exception;

class LockedBalanceChangeValueException extends Exception
{
    public function __construct(Balance $balance)
    {
        $balanceId = $balance->getKey();
        $message = "Can`t change value of locked balance. Balance id: $balanceId";
        parent::__construct($message);
    }
}
