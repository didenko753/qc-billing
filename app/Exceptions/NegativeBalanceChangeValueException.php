<?php

namespace App\Exceptions;

use App\Models\Balance;
use Exception;

class NegativeBalanceChangeValueException extends Exception
{
    public function __construct(Balance $balance)
    {
        $balanceId = $balance->getKey();
        $message = "Can`t set negative value of balance. Balance id: $balanceId";
        parent::__construct($message);
    }
}
