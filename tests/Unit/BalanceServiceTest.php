<?php

namespace Tests\Unit;

use App\Exceptions\LockedBalanceChangeValueException;
use App\Exceptions\NegativeBalanceChangeValueException;
use App\Models\Balance;
use App\Models\User;
use App\Services\Balance\BalanceService;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class BalanceServiceTest extends TestCase
{
    use DatabaseTransactions;
    use WithFaker;

    /**
     * @var BalanceService
     */
    private $balanceService;

    public function setUp(): void
    {
        parent::setUp();
        $this->balanceService = resolve(BalanceService::class);
    }

    /**
     * Тест зачисления
     */
    public function test_increase_balance()
    {
        /** @var User $user */
        $user = factory(User::class)->create();
        $amount = 500;
        $balanceOldValue = $user->balance->value;
        $this->balanceService->increaseUserBalance($user->balance, $amount);

        $this->assertTrue($user->balance->fresh()->value === $balanceOldValue + $amount);
    }

    /**
     * Тест зачисления на залоченный счет
     */
    public function test_increase_locked_balance()
    {
        $balance = $this->generateBalanceWithValue(5000);
        $amount = 500;

        $this->expectException(LockedBalanceChangeValueException::class);
        $this->balanceService->lockUserBalance($balance, $amount);
        $this->balanceService->increaseUserBalance($balance, $amount);
    }

    /**
     * Тест снятия средств
     */
    public function test_debit_balance()
    {
        $amount = 1200;
        $oldValue = 5000;
        $balance = $this->generateBalanceWithValue($oldValue);

        $this->balanceService->debetUserBalance($balance, $amount);
        $this->assertTrue(
            $balance->fresh()->value === $oldValue - $amount
        );
    }

    /**
     * Тест снятия с залоченного счета
     */
    public function test_debet_locked_balance()
    {
        $balance = $this->generateBalanceWithValue(5000);
        $amount = 500;

        $this->expectException(LockedBalanceChangeValueException::class);
        $this->balanceService->lockUserBalance($balance, $amount);
        $this->balanceService->debetUserBalance($balance, $amount);
    }

    /**
     * Тест изменения значения у залоченного счета
     */
    public function test_locked_balance_event()
    {
        $balance = $this->generateBalanceWithValue(5000);

        $this->balanceService->lockUserBalance($balance, rand(1, 100));
        $this->expectException(LockedBalanceChangeValueException::class);

        /**
         * В обсервере на событие сохранения
         * установлена проверка на изменение значения у залоченного счет
         */
        $balance->update(['value' => rand(1, 100)]);
    }

    /**
     * Тест снятия суммы,
     * превышающей остаток на счете
     */
    public function test_debet_big_value()
    {
        $balance = $this->generateBalanceWithValue(100);
        $this->expectException(NegativeBalanceChangeValueException::class);

        $this->balanceService->debetUserBalance($balance, 200);
    }


    /**
     * Тест перевода средств
     */
    public function test_transfer()
    {
        $balanceFrom = $this->generateBalanceWithValue(500);
        $balanceTo = $this->generateBalanceWithValue(100);

        $this->balanceService->transfer($balanceFrom, $balanceTo, 150);

        $this->assertTrue($balanceFrom->fresh()->value === 350);
        $this->assertTrue($balanceTo->fresh()->value === 250);
    }

    /**
     * Тест перевода на залоченный счет
     */
    public function test_transfer_locked()
    {
        $balanceFrom = $this->generateBalanceWithValue(500);
        $balanceTo = $this->generateBalanceWithValue(100);

        $this->balanceService->lockUserBalance($balanceTo, 50);

        $this->expectException(LockedBalanceChangeValueException::class);
        $this->balanceService->transfer($balanceFrom, $balanceTo, 150);
    }

    /**
     * Тест разблока счета со списанием
     */
    public function test_unlock_with_debit()
    {
        $balance = $this->generateBalanceWithValue(500);
        $this->balanceService->lockUserBalance($balance, 200);

        $this->balanceService->unlockDebetUserBalance($balance);

        $this->assertTrue($balance->fresh()->value === 300);
        $this->assertFalse($balance->fresh()->isLocked());
    }

    /**
     * Тест разблока счета без списания
     */
    public function test_unlock_with_rollback()
    {
        $value = 500;
        $balance = $this->generateBalanceWithValue($value);
        $this->balanceService->lockUserBalance($balance, 200);

        $this->balanceService->unlockRollbackUserBalance($balance);

        $this->assertTrue($balance->fresh()->value === $value);
        $this->assertFalse($balance->fresh()->isLocked());
    }




    /**
     * Сгенерить баланс с указанным значением
     *
     * @param int $value
     * @return Balance
     */
    protected function generateBalanceWithValue(int $value): Balance
    {
        /** @var User $user */
        $user = factory(User::class)->create();
        $balance = $user->balance;
        $balance->update(['value' => $value]);

        return $balance->fresh();
    }
}
