<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Events\UserBalanceDebitEvent;
use App\Events\UserBalanceIncreaseEvent;
use App\Events\UserBalanceLockEvent;
use App\Events\UserBalanceTransferEvent;
use App\Events\UserBalanceUnlockDebetEvent;
use App\Events\UserBalanceUnlockRollbackEvent;
use App\Models\User;
use App\Services\Balance\UserBalanceEventDTO;

Route::get('/', function () {
    /**
     * Код пример для публикации сообщений
     */
    $publishIncrease = function () {
        $user = User::first();
        $userBalanceDTO = new UserBalanceEventDTO($user);
        $userBalanceDTO->setAmount(300);

        $event = new UserBalanceIncreaseEvent($userBalanceDTO);
        publish($event);
    };

    $publishDebit = function () {
        $user = User::first();
        $userBalanceDTO = new UserBalanceEventDTO($user);
        $userBalanceDTO->setAmount(300);

        $event = new UserBalanceDebitEvent($userBalanceDTO);
        publish($event);
    };

    $publishTransfer = function () {
        $user = User::first();
        $recipient = User::find(2);

        $userBalanceDTO = new UserBalanceEventDTO($user);
        $userBalanceDTO
            ->setAmount(300)
            ->setRecipient($recipient);

        $event = new UserBalanceTransferEvent($userBalanceDTO);
        publish($event);
    };

    $publishLock = function () {
        $user = User::first();

        $userBalanceDTO = new UserBalanceEventDTO($user);
        $userBalanceDTO
            ->setAmount(300);

        $event = new UserBalanceLockEvent($userBalanceDTO);
        publish($event);
    };

    $publishUnlockLockDebet = function () {
        $user = User::first();

        $userBalanceDTO = new UserBalanceEventDTO($user);

        $event = new UserBalanceUnlockDebetEvent($userBalanceDTO);
        publish($event);
    };

    $publishUnlockLockRollback = function () {
        $user = User::first();

        $userBalanceDTO = new UserBalanceEventDTO($user);

        $event = new UserBalanceUnlockRollbackEvent($userBalanceDTO);
        publish($event);
    };


    return view('welcome');
});
