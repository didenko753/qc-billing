## Qc-Billing


### Перечень используемых инструментов и технологий

- Фремворк Laravel 5.8
- PHP 7.1
- MySql (Но без проблем заработает и на Postgree)
- RabbitMq
- Пакет для работы с рэббитом https://nuwber.github.io/rabbitevents/




## Cпособ развертки приложения

`git clone git@bitbucket.org:didenko753/qc-billing.git`

`cd qc-billing`

Создать базу, заполнить `.env` по примеру (.env.example)

`composer install`

`php artisan migrate` - Прогнать миграции

`php artisan db:seed` - Запустить сиды

Запустить слушателя событий из рэббита:

`php artisan rabbitevents:listen user_balance.*`

Запустить слушателя ответов из рэббита (необязательно):
 
`php artisan rabbitevents:listen user_balance_event_handled`

Запуск тестов:
`vendor/bin/phpunit`

### Настройка RabbtMq
`sudo rabbitmqctl add_vhost events` - создать виртуальный хост с именем `events`

## Общий механизм работы (интерфейсы ввода/вывода)

Пример пуша сообщений `routes/web.php`
Сообщения пушатся в рэббит, затем слушатель(слушатели зарегистированы в `Providers/RabbitEventsServiceProvider.php`) 
получает сообщение. События с ключом `user_balance.*` попадают в `\App\Listeners\UserBalanceListener`.

После успешной обработки публикуется сообщение-ответ. Для дебага/проверки можно запустить здесь же слушатель этой очереди.

`php artisan rabbitevents:listen user_balance_event_handled`

Также результаты выполнения или ошибки пишутся в лог:

`tail -f -n 300 storage/log/laravel.log`